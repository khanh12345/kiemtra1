﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;

namespace Ktrthu5.Models
{
    public class Reports
    {
        [Key]
        public int ReportID { get; set; }
        public int AccountID { get; set; }
        public Accounts? Account { get; set; }
        public int LogsID { get; set; }
        public Logs? Log { get; set; }
        public int TransactionalID { get; set; }
        public Transaction? Transactional { get; set; }

        public string Reportname { get; set; }
        public DateTime Reportdate { get; set; }

       
        

        

        

    }
}
