﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ktrthu5.Models
{
    public class Logs
    {
        [Key]
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public Transaction? Transactional { get; set; }
        public DateTime Logindate { get; set; }
        public DateTime Logintime { get; set; }




    }
}
