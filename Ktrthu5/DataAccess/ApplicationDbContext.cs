﻿using Ktrthu5.Models;
using Microsoft.EntityFrameworkCore;


namespace Ktrthu5.DataAccess
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Log)
                .WithMany()
                .HasForeignKey(r => r.LogsID)
                .OnDelete(DeleteBehavior.NoAction); // Thêm DeleteBehavior.NoAction vào đây


            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Transactional)
                .WithMany()
                .HasForeignKey(r => r.TransactionalID)
                .OnDelete(DeleteBehavior.NoAction); // Thêm DeleteBehavior.NoAction vào đây




            base.OnModelCreating(modelBuilder);
        }

    }
}
