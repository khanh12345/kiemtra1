﻿using System.ComponentModel.DataAnnotations;

namespace Ktrthu5.Models
{
    public class Employees
    {
        [Key]
        public int EmployeesID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Contact { get; set; }
        public string address { get; set; }
        public string Usename { get; set; }
        public string Password { get; set; }

       
    }
}
